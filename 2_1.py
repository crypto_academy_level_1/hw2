'''
The following program applies the xor operation to each English letter in a sequence of encrypted letters

using a key stream that is in the form of a sequence of English letters.

This will decrypt the ciphertext.

The English letters encode the numbers 0,...,25 for letters A,...,Z

All whitespace characters are left as is in the final plaintext.
'''

ciphertext = "bsaspp kkuosp"

key = "rsidpy dkawoy"

plaintext = ""

i = 0

while i < len(ciphertext):

    if ciphertext[i].isspace() == True:

        plaintext += ciphertext[i]

        i += 1

        continue

    cipher_letter = ( ord(ciphertext[i]) - ord('a') ) % 26

    key_letter = ( ord(key[i]) - ord('a') ) % 26

    xor_letter = ( cipher_letter - key_letter ) % 26
    
    plaintext += chr( xor_letter + ord('a')) 

    i += 1

print(plaintext)

