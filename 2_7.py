'''
The equation for LFSR for degree m = 8 and polynomial:

x^8 + x^4 + x^3 + x + 1

can be expressed as:

s_(i+8) := s_i ^ s_(i+1) ^ s_(i+3) ^ s_(i+4)

otherwise known as:

s_i := s_(i-8) ^ s_(i-7) ^ s_(i-5) ^ s_(i-4)
'''

'''
We will store all bits in a Python List

Since we must get the first two output bytes we will need

to compute through 16 * 8 = 128 bits to complete the computation.

All bits divisible by 8 are the output bits in reverse order.

So the final output vector must be reversed to get the correct answer.

The equation for s_i must be applied to all bits under FF_7.

All other LFSR bits are assigned based on the equation s_i[r][c] := s_i[r-1][c-1]

in the 2-D array s.
'''

rows, cols = 16, 8

s = [ [0 for i in range(cols)] for j in range(rows) ] # LSFR computation vector expressed as 2-D array

f = [None] * 16 # Final output vector of two bytes

print("Printing initialization vector table (expecting all elements to be 0:")

for i in range(rows):

    for j in range(cols):

        print(str(s[i][j]) + " ",end='')

    print('')


j = 0

while j < 8:

    s[0][j] = 1

    j += 1

print("Printing initialization vector table after initialization vector placed in top row:")

for i in range(rows):

    for j in range(cols):

        print(str(s[i][j]) + " ",end='')

    print('')

print("Computing bits for LFSR Flip Flops 0 - 7 in vector table starting from Row 1:")

i = 1

while i < rows:

    for j in range(cols):

#        print("i: " + str(i) + " j: " + str(j))

        if j == 0:

            s[i][j] = s[i-1][7] ^ s[i-1][6] ^ s[i-1][4] ^ s[i-1][3]

        else:

            s[i][j] = s[i-1][j-1]

    i += 1

print("Printing LFSR vector table:")

for i in range(rows):

    for j in range(cols):

        print(str(s[i][j]) + " ",end='')

    print('')


# Formatting vector f for two output bytes below

i = 15

j = 7

k = 0

while i >= 0:

    f[k] = s[i][j]

    i -= 1

    k += 1

print("Printing first two output bytes as a one-dimensional binary string:")

j = 0

while j < 16:

    print(str(f[j]) + " ",end='')

    j += 1

print('')

