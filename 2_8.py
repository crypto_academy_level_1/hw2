'''
The below code prints all sequences for x^4 + x + 1

for clock cycles 0 - 15, inclusive

Such a polynomial has the equation:

s_(i+4) := s_(i+1) ^ s_i

Such a primitive polynomial will produce enough sequences

The LFSR vector table starts with the following initialization vector:

FF_3    FF_2    FF_1    FF_0

1       0       0       0
'''
rows, cols = 16 , 4

primitive_polynomial = [ [0 for i in range(cols)] for j in range(rows) ] # LSFR computation vector expressed as 2-D array

primitive_polynomial[0][0] = 1 # Now the first row of the LSFR vector table is initialized

print("Printing initialization vector table after initialization vector placed in top row:")

for i in range(rows):

    for j in range(cols):

        print(str(primitive_polynomial[i][j]) + " ",end='')

    print('')

print("Computing bits for LFSR Flip Flops 0 - 3 in vector table starting from Row 1:")

i = 1

while i < rows:

    for j in range(cols):

        if j == 0:

            # s_(i+4) := s_(i+1) ^ s_i

            primitive_polynomial[i][j] = primitive_polynomial[i-1][2] ^ primitive_polynomial[i-1][3]

        else:

            primitive_polynomial[i][j] = primitive_polynomial[i-1][j-1]

    i += 1

print("Printing LFSR vector table for x^4 + x + 1:")

for i in range(rows):

    print("Clock Cycle " + str(i),end='')

    for j in range(cols):

        print(" & " + str(primitive_polynomial[i][j]),end='')

    print(" \\\\")

    print('')


'''
The below code prints all sequences for x^4 + x^3 + x^2 + x + 1

for clock cycles 0 - 15, inclusive

Such a polynomial has the equation:

s_(i+4) := s_(i+3) ^ s_(i+2) ^ s_(i+1) ^ s_(i)

Such a primitive polynomial will produce enough sequences

The LFSR vector table starts with the following initialization vector:

FF_3    FF_2    FF_1    FF_0

1       0       0       0
'''

rows, cols = 16 , 4

primitive_polynomial = [ [0 for i in range(cols)] for j in range(rows) ] # LSFR computation vector expressed as 2-D array

primitive_polynomial[0][0] = 1 # Now the first row of the LSFR vector table is initialized

print("Printing initialization vector table after initialization vector placed in top row:")

for i in range(rows):

    for j in range(cols):

        print(str(primitive_polynomial[i][j]) + " ",end='')

    print('')

print("Computing bits for LFSR Flip Flops 0 - 3 in vector table starting from Row 1:")

i = 1

while i < rows:

    for j in range(cols):

        if j == 0:

            # s_(i+4) := s_(i+1) ^ s_i

            primitive_polynomial[i][j] = primitive_polynomial[i-1][3] ^ primitive_polynomial[i-1][2] ^ primitive_polynomial[i-1][1] ^ primitive_polynomial[i-1][0]

        else:

            primitive_polynomial[i][j] = primitive_polynomial[i-1][j-1]

    i += 1

print("Printing LFSR vector table for x^4 + x^3 + x^2 + x + 1:")

for i in range(rows):

    print("Clock Cycle " + str(i),end='')

    for j in range(cols):

        print(" & " + str(primitive_polynomial[i][j]),end='')

    print(" \\\\")

    print('')


'''
The below code prints all sequences for x^4 + x^2 + 1

for clock cycles 0 - 15, inclusive

Such a polynomial has the equation:

s_(i+4) := s_(i+2) ^ s_(i+1)

Such a primitive polynomial will produce enough sequences

The LFSR vector table starts with the following initialization vector:

FF_3    FF_2    FF_1    FF_0

1       0       0       0
'''

rows, cols = 16 , 4

primitive_polynomial = [ [0 for i in range(cols)] for j in range(rows) ] # LSFR computation vector expressed as 2-D array

primitive_polynomial[0][0] = 1 # Now the first row of the LSFR vector table is initialized

print("Printing initialization vector table after initialization vector placed in top row:")

for i in range(rows):

    for j in range(cols):

        print(str(primitive_polynomial[i][j]) + " ",end='')

    print('')

print("Computing bits for LFSR Flip Flops 0 - 3 in vector table starting from Row 1:")

i = 1

while i < rows:

    for j in range(cols):

        if j == 0:

            # s_(i+4) := s_(i+2) ^ s_i

            primitive_polynomial[i][j] = primitive_polynomial[i-1][1] ^ primitive_polynomial[i-1][3]

        else:

            primitive_polynomial[i][j] = primitive_polynomial[i-1][j-1]

    i += 1

print("Printing LFSR vector table for x^4 + x + 1:")

for i in range(rows):

    print("Clock Cycle " + str(i),end='')

    for j in range(cols):

        print(" & " + str(primitive_polynomial[i][j]),end='')

    print(" \\\\")

    print('')
