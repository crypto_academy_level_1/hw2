'''
First we encode the plaintext "INTEL" into the binary form

such that all letters match the encoding table
'''

encoding = [None] * 32

i = 0

encoding[0] = 'A'

i += 1

while i < 26:

    encoding[i] = chr(ord(encoding[i-1]) + 1)

    i += 1

encoding[i] = '1'

i += 1

while i < 32:
    
    encoding[i] = chr(ord(encoding[i-1]) + 1)
    
    i += 1

plaintext_string = "INTEL"

plaintext_list = [None] * 5

i = 0

while i < 5:

    plaintext_list[i] = ord(plaintext_string[i]) - ord('A') 

    i += 1

'''
Print binary string encoding "INTEL" below:
'''

print("Printing string M as \"INTEL\" below as numbers:")

i = 0

while i < 5:

    print(str(plaintext_list[i]) + " ",end='')

    i += 1

print('')

print("Printing string M as \"INTEL\" below in binary form:")

i = 0

while i < 5:

    print(str("{0:b}".format(plaintext_list[i])) + " ",end='')

    i += 1

print('')


i = 0

'''
Next we store K in a Python List
'''

K = [ 
        0b11010 , 0b10010 , 0b00100 , 0b01011 , 

     0b10100 , 0b10111 , 0b00101 , 0b10010

     ]

'''
Now we apply the xor operation to the first five elements

of plaintext_list against the first five elements of K
'''

i = 0

C_1 = [None] * 5

while i < 5:

    C_1[i] = (plaintext_list[i] ^ K[i])

    i += 1

'''
Printing C_1 below as numbers
'''

i = 0

print("Printing C_1 below as numbers:")

while i < 5:

    print(str(C_1[i]) + " ",end='')

    i += 1

print('')


i = 0

print("Printing C_1 below as binary numbers:")

while i < 5:

    print(str("{0:b}".format(C_1[i])) + " ",end='')

    i += 1

print('')

'''
Printing C_1 below as letters
'''

print("Printing C_1 below as letters based on MyCode encoding:")

i = 0

while i < 5:
    
    print(encoding[C_1[i]],end='')
#    print(chr(ord(C_1[i]) + ord('A')),end='')

    i += 1

print('')

'''
Below we decrypt C_1 back into plaintext form. To do this we

should make a lookup table that encodes the letters and numbers

to their respective binary encoding.
'''

print("Decrypting C_1 back to plaintext message:")

i = 0

P_1 = [None] * 5

while i < 5:
    
    intermediate = ( (C_1[i]) ^ K[i] )

#    P_1[i] = chr(intermediate + ord('A'))

    P_1[i] = encoding[intermediate]

    i += 1

'''
Below we print the recovered plaintext P_1
'''

i = 0

while i < 5:

    print(P_1[i],end='')

    i += 1

print('')

print("Replacing first 5 bits of C_1 with 10101 which is the number 21:")

C_1_tick = [None] * 5

i = 0

C_1_tick[0] = 21

i += 1

while i < 5:

    C_1_tick[i] = C_1[i]

    i += 1


print("Printing C_1_tick as numbers below:")

i = 0

while i < 5:

    print(str(C_1_tick[i]) + " ",end='')

    i += 1

print('')


print("Printing C_1_tick as binary numbers below:")

i = 0

while i < 5:

    print(str("{0:b}".format(C_1_tick[i])) + " ",end='')

    i += 1

print('')

i = 0

print("Printing P_1_tick as numbers below:")

P_1_tick = [None] * 5

while i < 5:
    
    P_1_tick[i] = ( (C_1_tick[i]) ^ K[i] )

#    P_1[i] = chr(intermediate + ord('A'))

#    P_1_tick[i] = encoding[intermediate]

    i += 1

'''
Below we print the recovered plaintext P_1_tick
'''
print("Below we print the recovered plaintext P_1_tick as numbers:")

i = 0

while i < 5:

    print(str(P_1_tick[i]) + " ",end='')

    i += 1

print('')


print("Below we print the recovered plaintext P_1_tick as letters:")

i = 0

while i < 5:

    print(str(encoding[P_1_tick[i]]) + " ",end='')

    i += 1

print('')

